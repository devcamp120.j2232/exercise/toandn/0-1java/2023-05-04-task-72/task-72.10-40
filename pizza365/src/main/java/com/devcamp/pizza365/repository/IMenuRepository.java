package com.devcamp.pizza365.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.pizza365.model.CMenu;

public interface IMenuRepository extends JpaRepository<CMenu, Long> {
  Optional<CMenu> findById(long id);
}
