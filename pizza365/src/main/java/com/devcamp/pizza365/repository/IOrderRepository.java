package com.devcamp.pizza365.repository;

import java.util.*;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.pizza365.model.COrder;
import com.devcamp.pizza365.model.CRegion;

@Repository
public interface IOrderRepository extends JpaRepository<COrder, Long> {
    COrder findById(long id); // tìm theo order id

    Optional<COrder> findByOrderCode(String orderCode);
}
