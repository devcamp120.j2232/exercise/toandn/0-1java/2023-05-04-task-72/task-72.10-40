package com.devcamp.pizza365.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.pizza365.model.CUser;

@Repository
public interface IUserRepository extends JpaRepository<CUser, Long> {
    Optional<CUser> findById(long id);
}
