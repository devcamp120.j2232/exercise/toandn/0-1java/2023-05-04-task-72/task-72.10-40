package com.devcamp.pizza365.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.model.CDrink;
import com.devcamp.pizza365.repository.CountryRepository;

@Service
public class CountryService {
  @Autowired
  private CountryRepository countryRepository;

  public ArrayList<CCountry> getAllCountrys() {
    ArrayList<CCountry> listCountry = new ArrayList<>();
    countryRepository.findAll().forEach(listCountry::add);
    return listCountry;
  }

  public CCountry createCountry(CCountry cCountry) {
    try {
      CCountry savedRole = countryRepository.save(cCountry);
      return savedRole;
    } catch (Exception e) {
      return null;
    }
  }
}
