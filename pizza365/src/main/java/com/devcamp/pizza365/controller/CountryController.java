package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.service.CountryService;
import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.model.CDrink;
import com.devcamp.pizza365.model.CRegion;
import com.devcamp.pizza365.repository.CountryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/country")
public class CountryController {
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	CountryService countryService;

	// lấy danh sách country
	@GetMapping("/all")
	public ResponseEntity<List<CCountry>> getAllCountrys() {
		try {
			return new ResponseEntity(countryService.getAllCountrys(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// get country detail by id
	@GetMapping("/detail/{id}")
	public ResponseEntity<Object> getCountryById(@PathVariable(name = "id", required = true) long id) {
		Optional<CCountry> country = countryRepository.findById(id);
		if (country.isPresent()) {
			return new ResponseEntity<>(country, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@PostMapping("/create")
	public ResponseEntity<Object> createCountry(@Valid @RequestBody CCountry cCountry) {
		try {
			Optional<CCountry> _checkCountryCode = countryRepository.findByCountryCode(cCountry.getCountryCode());
			if (_checkCountryCode.isPresent()) {
				return ResponseEntity.unprocessableEntity().body("Code Country already exsit!");
			} else {
				return new ResponseEntity<>(countryService.createCountry(cCountry), HttpStatus.CREATED);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Country: " + e.getCause().getCause().getMessage());
		}
	}

	@CrossOrigin
	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateCountry(@PathVariable("id") long id, @Valid @RequestBody CCountry cCountry) {
		try {
			Optional<CCountry> country = countryRepository.findById(id);
			CCountry newCountry = country.get();
			newCountry.setCountryName(cCountry.getCountryName());
			newCountry.setCountryCode(cCountry.getCountryCode());
			newCountry.setRegions(cCountry.getRegions());
			CCountry savedCountry = countryRepository.save(newCountry);
			try {
				return new ResponseEntity<>(savedCountry, HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Update specified Country: " +
								e.getCause().getCause().getMessage());
			}
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
		try {
			countryRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/country/count")
	public long countCountry() {
		return countryRepository.count();
	}

	/**
	 * Viết method kiểm tra country có trong CSDL hay không: checkCountryById() sử
	 * dụng hàm existsById() của CountryRepository
	 * 
	 * @param id
	 * @return
	 */
	@CrossOrigin
	@GetMapping("/check/{id}")
	public boolean checkCountryById(@PathVariable Long id) {
		return countryRepository.existsById(id);
	}

	/**
	 * Tìm country có chứa giá trị trong country code
	 * 
	 * @param code
	 * @return
	 */
	@CrossOrigin
	@GetMapping("/containing-code/{code}")
	public List<CCountry> getCountryByContainingCode(@PathVariable String code) {
		return countryRepository.findByCountryCodeContaining(code);
	}

	@CrossOrigin
	@GetMapping("/country2")
	public List<CCountry> getCountryByCountryName(@RequestParam(value = "countryName") String countryName) {
		return countryRepository.findByCountryName(countryName);
	}

	@CrossOrigin
	@GetMapping("/country3")
	public CCountry getCountryByRegionCode(@RequestParam(value = "regionCode") String regionCode) {
		return countryRepository.findByRegionsRegionCode(regionCode);
	}

	@CrossOrigin
	@GetMapping("/country4")
	public List<CCountry> getCountryByRegionName(@RequestParam(value = "regionName") String regionName) {
		return countryRepository.findByRegionsRegionName(regionName);
	}
}
