package com.devcamp.pizza365.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.model.COrder;
import com.devcamp.pizza365.model.CRegion;
import com.devcamp.pizza365.model.CUser;
import com.devcamp.pizza365.repository.IOrderRepository;
import com.devcamp.pizza365.repository.IUserRepository;
import com.devcamp.pizza365.service.UserService;
import com.devcamp.pizza365.service.OrderService;

@RestController
@CrossOrigin
@RequestMapping("/v1/order")
public class OrderController {
    @Autowired
    private UserService userService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private IOrderRepository pIOrderRepository;
    @Autowired
    private IUserRepository pIUserRepository;

    // get all order list
    @GetMapping("/all")
    public ResponseEntity<List<COrder>> getAllOrdersApi() {
        try {
            return new ResponseEntity<>(orderService.getAllOrders(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    // get order by user id
    @GetMapping("/search/userId={id}")
    public ResponseEntity<Set<COrder>> getOrderByUserIdApi(@PathVariable(name = "id") long id) {
        try {
            Set<COrder> userOrders = userService.getOrderByUserId(id);
            if (userOrders != null) {
                return new ResponseEntity<>(userOrders, HttpStatus.OK);
            } else
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // get order by id
    @GetMapping("/detail/{id}")
    public COrder getOrderById(@PathVariable(name = "id") Long id) {
        if (pIOrderRepository.findById(id).isPresent())
            return pIOrderRepository.findById(id).get();
        else
            return null;
    }

    // create new order with user id
    @PostMapping("/create/{id}")
    public ResponseEntity<Object> createOrder(@PathVariable("id") long id, @Valid @RequestBody COrder pOrder) {
        try {
            Optional<CUser> user = pIUserRepository.findById(id);
            Optional<COrder> _checkOrderCode = pIOrderRepository.findByOrderCode(pOrder.getOrderCode());
            COrder newOrder = new COrder();
            if (_checkOrderCode.isPresent()) {
                return ResponseEntity.unprocessableEntity().body("code order already exsit!");
            } else {
                newOrder.setOrderCode(pOrder.getOrderCode());
                newOrder.setPizzaSize(pOrder.getPizzaSize());
                newOrder.setPizzaType(pOrder.getPizzaType());
                newOrder.setVoucherCode(pOrder.getVoucherCode());
                newOrder.setUser(user.get());
                newOrder.setPrice(pOrder.getPrice());
                newOrder.setPaid(pOrder.getPaid());
                newOrder.setCreated(new Date());
                newOrder.setUpdated(null);
                COrder _order = pIOrderRepository.save(newOrder);
                try {
                    return new ResponseEntity<>(_order, HttpStatus.CREATED);
                } catch (Exception e) {
                    return ResponseEntity.unprocessableEntity()
                            .body("Failed to create specified order: " + e.getCause().getMessage());
                }
            }

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // update order by id
    @PutMapping("/update/{userId}/{orderId}")
    public ResponseEntity<Object> updateOrder(@PathVariable(name = "userId") long userId,
            @PathVariable(name = "orderId") long orderId,
            @Valid @RequestBody COrder updateOrder) {
        try {
            Optional<CUser> user = pIUserRepository.findById(userId);
            COrder _order = pIOrderRepository.findById(orderId);
            _order.setPizzaSize(updateOrder.getPizzaSize());
            _order.setPizzaType(updateOrder.getPizzaType());
            _order.setVoucherCode(updateOrder.getVoucherCode());
            _order.setPrice(updateOrder.getPrice());
            _order.setPaid(updateOrder.getPaid());
            _order.setUpdated(new Date());
            _order.setUser(user.get());
            try {
                return ResponseEntity.ok(pIOrderRepository.save(_order));
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified order: " + e.getCause().getMessage());
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    // delete user by Id
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<COrder> deleteOrderById(@PathVariable("id") long id) {
        try {
            pIOrderRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
